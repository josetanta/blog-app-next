import { Typography } from '@material-ui/core';
import Layout from 'components/Layout';

function HomePage() {
	return (
		<Layout>
			<Typography variant='h1' component='h1' align='center'>
				Principal
			</Typography>
		</Layout>
	);
}

export default HomePage;
