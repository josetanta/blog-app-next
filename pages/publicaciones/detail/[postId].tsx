import { Fragment } from 'react';
import { usePostDetail } from 'hooks/usePostQuery';
import { useRouter } from 'next/router';
import NextImage from 'next/image';

import Layout from 'components/Layout';
import { Box, Container, Paper, Typography } from '@material-ui/core';
import { motion } from 'framer-motion';
import CommentList from 'components/CommentList';
import marked from 'marked';

function PostDetailPage() {
	const { postId } = useRouter().query;
	const postDetail = usePostDetail(postId as string);
	const post = postDetail.data?.post;
	return (
		<Layout title='Publicación Detalle'>
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				transition={{
					delay: 1,
					x: {
						type: 'spring',
						stiffness: 100,
					},
					default: { duration: 1 },
				}}
			>
				<Container maxWidth='xl'>
					<Typography
						variant='h1'
						component='h1'
						align='center'
						color='primary'
						sx={{
							fontWeight: 'bold',
						}}
					>
						{post?.title}
					</Typography>
					<Box display='flex' justifyContent='center'>
						{post?.image.url_image.length && (
							<NextImage
								layout='intrinsic'
								width={1000}
								height={500}
								objectFit='contain'
								src={post?.image.url_image}
								alt={post?.title}
							/>
						)}
					</Box>

					<Paper
						elevation={3}
						sx={{
							p: 5,
							letterSpacing: 0.6,
							lineHeight: 2,
						}}
					>
						<Typography
							component='div'
							variant='body1'
							textAlign='justify'
							dangerouslySetInnerHTML={{ __html: marked(post?.content || '') }}
						/>
					</Paper>
				</Container>
				{post?.relations?.comments && (
					<Fragment>
						<Typography sx={{ mt: 5 }} variant='h6' align='center'>
							Comentarios
						</Typography>
						<CommentList comments={post?.relations?.comments} />
					</Fragment>
				)}
			</motion.div>
		</Layout>
	);
}

export default PostDetailPage;
