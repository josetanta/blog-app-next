// Components
import { withRouter, NextRouter } from 'next/router';
import { useAuth } from 'hooks/useStores';

import Layout from 'components/Layout';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ListPost from 'components/ListPost';

function PostPrincipal(props: { router: NextRouter }) {
	const [auth] = useAuth();
	const router = props.router;

	const handleRouteClick = async () => {
		if (auth.isAuthenticated) await router.push('/publicaciones/new-post');
		else await router.push('/sign/in?redirect=/publicaciones/new-post');
	};

	return (
		<Layout title='Publicaciones'>
			<Typography variant='h1' component='h1' align='center'>
				Publicaciones
			</Typography>
			<Box display='flex' justifyContent='center'>
				<Button onClick={handleRouteClick} variant='outlined'>
					Crear una publicación
				</Button>
			</Box>
			<ListPost />
		</Layout>
	);
}

export default withRouter(PostPrincipal);
