import { useRef } from 'react';
import { useIsMutating } from 'react-query';
import { NextRouter } from 'next/router';

import { Box, Button, IconButton, Typography } from '@material-ui/core';
import PostForm from 'components/ui/PostForm';
import Layout from 'components/Layout';

// Icons
import CheckIcon from '@material-ui/icons/Check';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import withPrivateRoute from 'wrappers/withPrivateRoute';

function CreatePostPage(props: { router: NextRouter }) {
	const btnRef = useRef<HTMLButtonElement>(null);
	const router = props.router;
	const isMutating = useIsMutating('CREATE_POST');

	const handleBtnSubmitClick = async () => {
		btnRef.current?.click();
		!isMutating && (await router.push('/publicaciones'));
	};

	return (
		<Layout title='Crear una publicación'>
			<Box
				sx={{
					p: 3,
					mb: 3,
				}}
				display='flex'
				justifyContent='space-between'
				alignItems='center'
			>
				<Typography variant='h2' component='h2' align='center'>
					Crear nueva publicación
				</Typography>
				<div>
					<Button variant='contained' startIcon={<CheckIcon />} onClick={handleBtnSubmitClick}>
						Guardar
					</Button>
					<IconButton color='error'>
						<HighlightOffIcon />
					</IconButton>
				</div>
			</Box>
			<Box
				sx={{
					p: 3,
				}}
			>
				<Box>
					<PostForm ref={btnRef} />
				</Box>
			</Box>
		</Layout>
	);
}

export default withPrivateRoute(CreatePostPage);
