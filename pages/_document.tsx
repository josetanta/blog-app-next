import { Children } from 'react';
import { ServerStyleSheets } from '@material-ui/styles';
// eslint-disable-next-line @next/next/no-document-import-in-page
import DocumentNext, { DocumentContext, Head, Html, Main, NextScript } from 'next/document';

class AppDocument extends DocumentNext {
	static async getInitialProps(ctx: DocumentContext) {
		const sheets = new ServerStyleSheets();
		const originalRenderPage = ctx.renderPage;

		ctx.renderPage = async () =>
			await originalRenderPage({
				enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
				enhanceComponent: (Component) => Component,
			});

		const initialProps = await DocumentNext.getInitialProps(ctx);

		return {
			...initialProps,
			styles: [...Children.toArray(initialProps.styles), sheets.getStyleElement()],
		};
	}

	render() {
		return (
			<Html lang='es'>
				<Head>
					<meta charSet='utf-8' />
					<meta name='theme-color' content='#808080' />
					<meta name='description' content='Web site created using create-react-app' />
					<link rel='preconnect' href='https://fonts.googleapis.com' />
					<link rel='preconnect' href='https://fonts.gstatic.com' crossOrigin='true' />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default AppDocument;
