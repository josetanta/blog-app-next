import { Box, Typography } from '@material-ui/core';
import Layout from 'components/Layout';
import AccountForm from 'components/ui/AccountForm';
import AlertMessage from 'components/ui/AlertMessage';
import UserToolbarLayout from 'components/user/UserToolbarLayout';
import { useAuth } from 'hooks/useStores';
import withPrivateRoute from 'wrappers/withPrivateRoute';

function AccountConfigPage() {
	const [auth, getState] = useAuth();
	return (
		<Layout title='Configurar mi cuenta'>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100%',
					}}
					display='flex'
					flexDirection='column'
					justifyContent='space-evenly'
				>
					<Typography
						component='h3'
						variant='h3'
						sx={{
							px: 3,
						}}
					>
						Actualizar mi cuenta de usuario
					</Typography>
					<AlertMessage
						status='error'
						message={getState()?.message}
						onClose={auth.setResetResponse}
						severity='error'
					/>
					<AlertMessage
						status='updated'
						message={getState()?.message}
						onClose={auth.setResetResponse}
						severity='success'
					/>
					<AccountForm />
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default withPrivateRoute(AccountConfigPage);
