import type { NextRouter } from 'next/router';
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'

import Layout from 'components/Layout';
import { useAuth } from 'hooks/useStores';

import * as paths from 'utils/paths-routes';
import UserToolbarLayout from 'components/user/UserToolbarLayout';
import withPrivateRoute from 'wrappers/withPrivateRoute';

function AccountPage(props: { router: NextRouter }) {
	const router = props.router;
	const [auth] = useAuth();
	const handlePushRouteClick = async () => {
		await router.push(`${paths.USER_PRINCIPAL}${paths.USER_SETTINGS}`);
	};

	return (
		<Layout title='Perfil'>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100vh',
					}}
					display='flex'
					flexDirection='column'
					justifyContent='space-evenly'
				>
					<Typography
						component='h3'
						variant='h3'
						sx={{
							px: 3,
						}}
					>
						Información de mi cuenta
					</Typography>
					<Table>
						<TableBody>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Nombre completo
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.username}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Usuario
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.username}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Correo
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.email}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Teléfono
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.phone}
								</TableCell>
							</TableRow>
						</TableBody>
					</Table>
					<Button color='inherit' onClick={handlePushRouteClick}>
						Editar mi cuenta
					</Button>
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default withPrivateRoute(AccountPage);
