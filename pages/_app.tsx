import type { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import GeneralProvider from 'providers/index';
import CheckConnection from 'components/CheckConnection';
import useMounted from 'hooks/useMounted';

import 'styles/main.css'

const AppRoot = dynamic(() => import('components/AppRoot'), {
	ssr: false,
});

function AppBlog(props: AppProps) {
	let { Component, pageProps, router } = props;

	useMounted(() => {
		const jsxStyles = document.querySelector('#jss-server-side');

		if (jsxStyles) {
			jsxStyles.parentElement?.removeChild(jsxStyles);
		}
	});

	return (
		<GeneralProvider pageProps={pageProps}>
			<AppRoot>
				<CheckConnection>
					<Component {...pageProps} key={router.route} />
				</CheckConnection>
			</AppRoot>
		</GeneralProvider>
	);
}

export default AppBlog;
