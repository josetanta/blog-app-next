import { Typography } from '@material-ui/core';
import Layout from 'src/components/Layout';

function AboutPage() {
	return (
		<Layout title='Sobre Nosotros'>
			<Typography variant='h3' component='h3' align='center'>
				Sobre Nosotros
			</Typography>
		</Layout>
	);
}

export default AboutPage;
