declare module 'required-params' {
	export type LoginData = { email: string; password: string };
}

declare module 'store-app' {
	import { UserAccountResponse } from 'response-models';
	import { LoginData } from 'required-params';
	type UserRegister = {
		username: string;
		email: string;
		password: string;
		repeatPassword: string;
	};

	export type AuthStore = {
		user: UserAccountResponse['user'];
		isAuthenticated: boolean;
		message?: object | string | string[] | object[];
		authName?: string;
		token: string;

		status?: 'loading' | 'error' | 'updated' | 'success';
		isReady?: boolean;

		setLoginAuth(data: LoginData): Promise<void> | void;
		setLogoutAuth(): Promise<void> | void;
		setUpdateAccount(value: any | object): Promise<void>;
		setRegisterUser(data: UserRegister): Promise<void> | void;
		setReset(): Promise<void> | void;
		setResetResponse(): Promise<void> | void;
		setLogin0Auth(): Promise<void> | void;
		getUserAccount(token: string): Promise<void> | void;
	};
}
