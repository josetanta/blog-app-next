module.exports = {
	cssModules: true,
	reactStrictMode: true,
	images: {
		domains: [
			'avatars.githubusercontent.com',
			'lh3.googleusercontent.com',
			'jtblog-api-rest.herokuapp.com',
			'via.placeholder.com',
			'127.0.0.1',
			'127.0.0.1:5000',
			'localhost',
		],
		deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
	},
};
