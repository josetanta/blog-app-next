import { Fragment, ReactNode } from 'react';
import NextHead from 'next/head';
import { useAuth } from 'hooks/useStores';
import { Typography } from '@material-ui/core';
import useMounted from 'hooks/useMounted';
import { getTokenAuth } from 'api/authApi';
import { setTokenStorage } from 'src/utils';

function AppRoot(props: { children: ReactNode }) {
	const [auth] = useAuth();

	useMounted(async (isMounted) => {
		if (isMounted) {
			await auth.getUserAccount(auth.token);

			let resAuth = await getTokenAuth();
			setTokenStorage(resAuth.data.token);
		}
	});

	if (!auth.isReady) {
		return (
			<Fragment>
				<NextHead>
					<title>Cargando...</title>
				</NextHead>
				<Typography variant='h1' align='center' marginY='auto'>
					Cargando...
				</Typography>
			</Fragment>
		);
	}

	return <Fragment>{props.children}</Fragment>;
}

export default AppRoot;
