import { Form, Formik } from 'formik';
import { Box, Button, CircularProgress, Paper } from '@material-ui/core';
import { useAuth } from 'hooks/useStores';
import AlertMessage from './AlertMessage';
import FormField from 'components/FormField';
import * as Yup from 'yup';

import LoginIcon from '@material-ui/icons/Login';
import { useRouter } from 'next/router';

type FormValues = {
	email: string;
	password: string;
};

const initialValues: FormValues = {
	email: '',
	password: '',
};

function LoginForm() {
	const [auth, getState] = useAuth();
	const router = useRouter();

	return (
		<Paper
			sx={{
				p: 2,
				mt: 2,
			}}
			elevation={2}
		>
			<Formik<FormValues>
				initialValues={initialValues}
				validationSchema={Yup.object({
					email: Yup.string()
						.email('Por favor el correo debe ser correcto.')
						.required('El correo es requerido.'),
					password: Yup.string().required('La contraseña es requerida'),
				})}
				onSubmit={async (values) => {
					await auth.setLoginAuth(values);
					if (auth.isAuthenticated) await router.push(`${router.query.redirect}`);
				}}
			>
				{({ isValid, isSubmitting }) => (
					<Form aria-label='login-form'>
						<Box display='flex' flexDirection='column'>
							<AlertMessage
								status='error'
								message={getState()?.message}
								onClose={auth.setReset}
								severity='error'
							/>
							<AlertMessage
								status='success'
								message={getState()?.message}
								onClose={auth.setReset}
								severity='success'
							/>

							<Box component='div' sx={{ m: 2 }}>
								<FormField autoFocus required type='email' label='Correo' name='email' />
							</Box>

							<Box component='div' sx={{ m: 2 }}>
								<FormField required type='password' name='password' label='Contraseña' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<Button
									startIcon={<LoginIcon />}
									fullWidth
									type='submit'
									variant='contained'
									disabled={getState().status === 'loading' || !isValid || isSubmitting}
								>
									{(getState().status === 'loading' && (
										<CircularProgress title='Ingresar' size={30} />
									)) ||
										'Ingresar'}
								</Button>
							</Box>
						</Box>
					</Form>
				)}
			</Formik>
		</Paper>
	);
}

export default LoginForm;
