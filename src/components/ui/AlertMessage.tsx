import { AuthStore } from 'store-app';
import { useAuth } from 'hooks/useStores';
import withError from 'wrappers/withError';
import {
	Alert,
	AlertProps,
	Box,
	Collapse,
	IconButton,
	ListItem,
	ListItemText,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

type AlertMessageProps = AlertProps & {
	onClose(): void;
	message: Readonly<AuthStore['message']>;
	status?: Readonly<AuthStore['status']>;
	show?: boolean;
};

/**
 * Component Alert for message any errors
 * from store
 */
function AlertMessage(props: AlertMessageProps) {
	const { onClose, message, status, show, ...restProps } = props;
	const getState = useAuth()[1];
	return (
		<Collapse in={show || getState().status === status}>
			<Box component='div' sx={{ m: 2 }}>
				<Alert
					{...restProps}
					action={
						<IconButton aria-label='close' color='inherit' size='small' onClick={onClose}>
							<CloseIcon fontSize='inherit' />
						</IconButton>
					}
				>
					{(message instanceof Array &&
						message.map((obj) => {
							if (obj instanceof Object) {
								let renderObj: Record<string, string> = { ...obj };
								return Object.keys(renderObj).map((key, index) => (
									<ListItem key={`error-${index}`}>
										{renderObj[key].length > 1 && <ListItemText primary={renderObj[key]} />}
									</ListItem>
								));
							} else {
								return (message as Array<string>).map((text, index) => (
									<ListItem key={`error-${index}`}>
										<ListItemText primary={text} />
									</ListItem>
								));
							}
						})) ||
						message}
				</Alert>
			</Box>
		</Collapse>
	);
}

export default withError(AlertMessage);
