import { Form, Formik } from 'formik';
import { Box, Button, CircularProgress, Paper } from '@material-ui/core';
import * as Yup from 'yup';
import FormField from 'components/FormField';
import AlertMessage from './AlertMessage';
import { useAuth } from 'hooks/useStores';

type RegisterFormProps = {
	// eslint-disable-next-line no-unused-vars
	onChange(v: number): void;
};

type ValueForm = {
	username: string;
	email: string;
	password: string;
	repeatPassword: string;
};

const initValues: ValueForm = {
	username: '',
	email: '',
	password: '',
	repeatPassword: '',
};

function RegisterForm(props: RegisterFormProps) {
	const [auth, getState] = useAuth();
	return (
		<Paper
			sx={{
				p: 2,
				mt: 2,
			}}
			elevation={2}
		>
			<Formik<ValueForm>
				initialValues={initValues}
				onSubmit={async (values) => {
					await auth?.setRegisterUser(values);
					if (getState().status == 'success') {
						await Promise.resolve(props.onChange(0));
					}
				}}
				validationSchema={Yup.object({
					email: Yup.string()
						.email('Por favor escribe un correo válido')
						.required('Se require este campo de correo'),
					username: Yup.string()
						.min(5, 'Como mínimo se requiere 5 caracteres')
						.required('Se require este campo de nombre de usuario'),
					password: Yup.string()
						.min(8, 'La nueva contraseña como mínimo debe ser de 8 caracteres.')
						.required('Se require la contraseña.'),
					repeatPassword: Yup.string()
						.equals([Yup.ref('password')], 'Las contraseñas deben de ser iguales.')
						.required('Es requerido repetir la contraseña'),
				})}
			>
				{({ isValid }) => (
					<Form>
						<Box display='flex' flexDirection='column'>
							<AlertMessage
								status='error'
								message={getState()?.message}
								onClose={auth.setReset}
								severity='error'
								variant='outlined'
							/>

							<AlertMessage
								status='success'
								message={getState()?.message}
								onClose={auth.setReset}
								severity='success'
								variant='outlined'
							/>

							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='email' label='Correo' type='email' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='username' label='Nombre de usuario' type='text' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='password' label='Contraseña' type='password' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField
									required
									name='repeatPassword'
									label='Repite la contraseña'
									type='password'
								/>
							</Box>

							<Box component='div' sx={{ m: 2 }}>
								<Button
									type='submit'
									disabled={getState().status == 'loading' || !isValid}
									fullWidth
									variant='contained'
								>
									{(getState().status == 'loading' && (
										<CircularProgress title='Registrarse' size={30} />
									)) ||
										'Registrarse con esta cuenta'}
								</Button>
							</Box>
						</Box>
					</Form>
				)}
			</Formik>
		</Paper>
	);
}

export default RegisterForm;
