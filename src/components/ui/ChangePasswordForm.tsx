import { Box, Button } from '@material-ui/core';
import FormField from 'components/FormField';
import { Form, Formik } from 'formik';

import * as Yup from 'yup';

type FormValues = {
	currentPassword: string;
	newPassword: string;
	repeatNewPassword: string;
};

const initValues: FormValues = {
	currentPassword: '',
	newPassword: '',
	repeatNewPassword: '',
};

function ChangePasswordForm() {
	return (
		<Formik<FormValues>
			onSubmit={async (values, actions) => {
				actions.setSubmitting(true);
				console.log(values);
				actions.setSubmitting(false);
			}}
			initialValues={initValues}
			validationSchema={Yup.object({
				currentPassword: Yup.string().required('Por favor ponga su contraseña actual.'),
				newPassword: Yup.string()
					.min(8, 'La nueva contraseña como mínimo debe ser de 8 caracteres.')
					.required('Se require la nueva contraseña.'),
				repeatNewPassword: Yup.string()
					.equals([Yup.ref('newPassword')], 'Las contraseñas deben de ser iguales.')
					.required('Es requerido repetir la nueva contraseña'),
			})}
		>
			{({ isSubmitting }) => (
				<Form>
					<Box display='flex' flexDirection='column'>
						<Box component='div' sx={{ m: 2 }}>
							<FormField
								required
								name='currentPassword'
								type='password'
								label='Contraseña actual'
							/>
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<FormField required name='newPassword' type='password' label='Nueva contraseña' />
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<FormField
								required
								name='repeatNewPassword'
								type='password'
								label='Repetir la nueva contraseña'
							/>
						</Box>

						<Box component='div' sx={{ m: 2 }}>
							<Button disabled={isSubmitting} type='submit' fullWidth variant='contained'>
								Establecer nueva contraseña
							</Button>
						</Box>
					</Box>
				</Form>
			)}
		</Formik>
	);
}

export default ChangePasswordForm;
