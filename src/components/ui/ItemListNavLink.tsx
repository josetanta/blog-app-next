import { ReactNode } from 'react';
import NextLink from 'next/link';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

type ItemListNLProps = { nameItem: string; href: string; icon?: ReactNode };

function ItemListNavLink(props: ItemListNLProps) {
	return (
		<NextLink href={props.href} passHref>
			<ListItem button>
				<ListItemIcon>{props.icon}</ListItemIcon>
				<ListItemText primary={props.nameItem} />
			</ListItem>
		</NextLink>
	);
}

export default ItemListNavLink;
