import { forwardRef, memo, Ref, useState } from 'react';
import { useRouter } from 'next/router';
import NextDynamic from 'next/dynamic';

import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { usePostCreateMutate } from 'hooks/usePostQuery';
import { Box, Button, CircularProgress } from '@material-ui/core';
import FormField from 'components/FormField';

import ImageDragDrop from './ImageDragDrop';
import { valuesObject } from 'utils/formData';

const JoditEditor = NextDynamic(() => import('jodit-react'), {
	ssr: false,
});

type Values = {
	title: string;
	content: string;
};

const initValues: Values = {
	title: '',
	content: '',
};

function PostForm(_props: any, btnRef: Ref<HTMLButtonElement>) {
	const [file, setFile] = useState<File>();
	const postMutate = usePostCreateMutate();
	const router = useRouter();

	const [valueRichText, setValueRichText] = useState(initValues.content);

	return (
		<Formik<Values>
			initialValues={initValues}
			onSubmit={(values, actions) => {
				actions.setSubmitting(true);
				// form data
				const formData = valuesObject(values);
				if (file) {
					formData.append('image', file);
				}
				formData.append('content', valueRichText);
				postMutate.mutate(formData);
				actions.setSubmitting(false);
				router.push('/publicaciones');
			}}
			validationSchema={Yup.object({
				title: Yup.string()
					.required('Se require un titulo para la publicación')
					.min(12, 'Se requirere un mínimo de 12 caracteres'),
			})}
		>
			{({ isSubmitting, values }) => (
				<Form>
					<Box display='flex' flexDirection='column'>
						{isSubmitting && postMutate.isLoading && (
							<Box sx={{ m: 2 }}>
								<CircularProgress title='Creando la publicación' size={30} />
							</Box>
						)}
						<Box
							display='flex'
							flexDirection='column'
							justifyContent='space-content'
							sx={{ m: 2, mb: 2 }}
						>
							<ImageDragDrop imgHeight='90%' file={file} setFile={setFile} text={values.title} />
						</Box>
						<Box sx={{ m: 2 }}>
							<FormField name='title' type='text' label='Titulo de la publicación' />
						</Box>
						<Box sx={{ m: 2 }}>
							<Box
								sx={{
									boxShadow: 2,
									py: 1,
									px: 2,
									mt: 2,
								}}
							>
								<JoditEditor onBlur={(v) => setValueRichText(v)} value={valueRichText} />
							</Box>
						</Box>
						<Box sx={{ m: 2 }}>
							<Button
								ref={btnRef}
								type='submit'
								fullWidth
								variant='contained'
								disabled={isSubmitting}
							>
								{(isSubmitting && <CircularProgress title='Creando la publicación' size={30} />) ||
									'Crear la publicación'}
							</Button>
						</Box>
					</Box>
				</Form>
			)}
		</Formik>
	);
}

export default memo(forwardRef<HTMLButtonElement>(PostForm));
