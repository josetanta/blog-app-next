import { Fragment, ReactNode, useState } from 'react';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import NextHead from 'next/head';

import * as paths from 'utils/paths-routes';
import {
	alpha,
	Box,
	Button,
	Container,
	Divider,
	Drawer,
	IconButton,
	InputBase,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Toolbar,
	Typography,
} from '@material-ui/core';

import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@material-ui/core/AppBar';
import MenuItems from 'components/ui/MenuItems';
import { motion } from 'framer-motion';
import { styled, useTheme } from '@material-ui/core/styles';
import LoaderGlobal from 'components/LoaderGlobal';

// Icons
import SearchIcon from '@material-ui/icons/Search';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ShareIcon from '@material-ui/icons/Share';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import LoginIcon from '@material-ui/icons/Login';
import ViewAgendaIcon from '@material-ui/icons/ViewAgenda';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

// Hooks
import useStylesApp from 'utils/styles';

// Drawer Components
const drawerWidth = 240;
const Main = styled(motion.main, {
	shouldForwardProp: (prop) => prop !== 'open',
})<{
	open?: boolean;
}>(({ theme, open }) => ({
	flexGrow: 1,
	padding: theme.spacing(3),
	transition: theme.transitions.create('margin', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	marginLeft: `-${drawerWidth}px`,
	...(open && {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	}),
}));

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== 'open',
})<
	MuiAppBarProps & {
		open?: boolean;
	}
>(({ theme, open }) => ({
	transition: theme.transitions.create(['margin', 'width'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: `${drawerWidth}px`,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
	display: 'flex',
	alignItems: 'center',
	padding: theme.spacing(0, 1),
	...theme.mixins.toolbar,
	justifyContent: 'flex-end',
}));

const Search = styled('div')(({ theme }) => ({
	position: 'relative',
	borderRadius: theme.shape.borderRadius,
	backgroundColor: alpha(theme.palette.common.white, 0.15),
	'&:hover': {
		backgroundColor: alpha(theme.palette.common.white, 0.25),
	},
	marginLeft: 0,
	width: '100%',
	[theme.breakpoints.up('sm')]: {
		marginLeft: theme.spacing(1),
		width: 'auto',
	},
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
	padding: theme.spacing(0, 2),
	height: '100%',
	position: 'absolute',
	pointerEvents: 'none',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
	color: 'inherit',
	backgroundColor: alpha(theme.palette.primary.main, 0.2),
	'& .MuiInputBase-input': {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)})`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			width: '7ch',
			'&:focus': {
				width: '14ch',
			},
		},
	},
}));

type LayoutProps = { children: ReactNode; title?: string };

const links = [
	{ path: paths.HOME, title: 'Inicio', icon: <HomeIcon /> },
	{ path: '/sign/in', title: 'Iniciar Sesión', icon: <LoginIcon /> },
	{ path: paths.POST_PRINCIPAL, title: 'Publicaciones', icon: <ViewAgendaIcon /> },
	{ path: paths.ABOUT, title: 'Sobre nosotros', icon: <InfoIcon /> },
];

function Layout(props: LayoutProps) {
	const router = useRouter();
	const theme = useTheme();
	const classes = useStylesApp({});
	const [open, setOpen] = useState(false);

	const handleDrawerToggleClick = () => {
		setOpen((s) => !s);
	};

	const handlePushRouteClick = async () => {
		await router.push(paths.HOME);
	};

	return (
		<Fragment>
			<NextHead>
				<title>{props.title || 'Inicio'}</title>
				<meta title={props.title || 'Inicio'} />
			</NextHead>
			<Box sx={{ display: 'flex' }}>
				<AppBar position='fixed' open={open}>
					<Toolbar>
						<IconButton
							color='inherit'
							aria-label='open drawer'
							onClick={handleDrawerToggleClick}
							edge='start'
							sx={{ mr: 2, ...(open && { display: 'none' }) }}
						>
							<MenuIcon />
						</IconButton>

						<Button color='inherit' onClick={handlePushRouteClick}>
							<Typography variant='h6' component='h6' noWrap>
								My Blog
							</Typography>
						</Button>

						<div className={classes.grow} />
						<IconButton color='inherit'>
							<ShareIcon />
						</IconButton>
						<IconButton color='inherit'>
							<NotificationsIcon />
						</IconButton>
						<Search sx={{ mr: 2 }}>
							<SearchIconWrapper>
								<SearchIcon />
							</SearchIconWrapper>
							<StyledInputBase placeholder='Buscar...' inputProps={{ 'aria-label': 'search' }} />
						</Search>
						<MenuItems
							id='composition-button-menu-item'
							titleMenu='Usuario'
							icon={<AccountCircleIcon />}
						/>
					</Toolbar>
				</AppBar>
				<Drawer
					sx={{
						width: drawerWidth,
						flexShrink: 0,
						'& .MuiDrawer-paper': {
							width: drawerWidth,
							boxSizing: 'border-box',
							bgcolor: alpha(theme.palette.primary.main, 0.85),
						},
					}}
					variant='persistent'
					anchor='left'
					open={open}
				>
					<DrawerHeader>
						<IconButton onClick={handleDrawerToggleClick} color='inherit'>
							{theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
						</IconButton>
					</DrawerHeader>

					<Divider />
					<List>
						{links.map((link, index) => (
							<ListItem button key={`${link.title.toLowerCase()}-${index + 1}`}>
								<NextLink href={link.path} passHref>
									<div className={classes.linkDrawer}>
										<ListItemIcon>{link.icon}</ListItemIcon>
										<ListItemText primary={link.title} sx={{ color: '#fff' }} />
									</div>
								</NextLink>
							</ListItem>
						))}
					</List>
				</Drawer>
				<LoaderGlobal />
				<Main animate exit={{ opacity: 0 }}>
					<DrawerHeader />
					<Container>{props.children}</Container>
				</Main>
			</Box>
		</Fragment>
	);
}

export default Layout;
