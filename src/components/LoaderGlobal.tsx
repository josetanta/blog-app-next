import { useIsFetching, useIsMutating } from 'react-query';
import { CircularProgress } from '@material-ui/core';

function LoaderGlobal() {
	const isFetching = Boolean(useIsFetching());
	const isMutating = Boolean(useIsMutating());
	return (
		<CircularProgress
			sx={{
				position: 'absolute',
				top: '5rem',
				right: '2rem',
				animationDuration: '0.5s',
				visibility: isFetching || isMutating ? 'visible' : 'hidden',
			}}
			size={20}
			color='primary'
		/>
	);
}

export default LoaderGlobal;
