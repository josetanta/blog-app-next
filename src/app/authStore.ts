import create from 'zustand';
import * as authApi from 'api/authApi';
import * as userApi from 'api/userApi';

import { getTokenSessionStorage, setTokenStorage } from 'src/utils';
import { AuthStore } from 'store-app';

const authStore = create<AuthStore>((set, get) => ({
	isAuthenticated: false,
	isReady: false,
	user: null,
	token: getTokenSessionStorage(),

	async setLoginAuth(data) {
		set({ status: 'loading' });
		const resLogin = await authApi.authLogin(data);
		if (resLogin.status !== 200) {
			set({
				message: resLogin.data.message,
				status: 'error',
			});
		} else {
			this.setReset();
			set({ token: resLogin.data.token });
			await this.getUserAccount(resLogin.data.token);
		}
	},
	async getUserAccount(token: string) {
		let res = await authApi.authGetUserAccount(token);
		set((state) => {
			if (res.status === 200)
				return {
					...state,
					user: res.data.user,
					isAuthenticated: Boolean(res.data.user?.email),
					isReady: Boolean(res.data.user),
				};
			else return { ...state, user: null, isAuthenticated: false, isReady: true };
		});
	},
	async setLogin0Auth() {
		set({ status: 'loading' });
		const res = await authApi.authGetUserAccount(get().token);
		if (res.status !== 200) {
			set({
				message: res.data.message,
				status: 'error',
			});
		} else {
			this.setReset();
			set({
				user: res.data.user,
				isAuthenticated: Boolean(res.data.user),
			});
		}
	},
	async setRegisterUser(data) {
		set({ status: 'loading' });
		const res = await authApi.authRegister(data);
		if (res.status !== 201) {
			set({
				message: res.data.message,
				status: 'error',
			});
		} else {
			this.setReset();
			set({
				message: res.data.message,
				status: 'success',
			});
		}
	},

	async setLogoutAuth() {
		await authApi.authLogout(get().authName);
		await this.setReset();
	},

	async setUpdateAccount(values) {
		set({ status: 'loading' });
		const res = await userApi.accountUpdate(values);
		if (res.status !== 200) {
			set(() => ({
				message: res.data.message,
				status: 'error',
			}));
		} else {
			set(() => ({
				user: res.data.user,
				message: res.data.message,
				status: 'updated',
			}));
		}
	},

	setResetResponse() {
		set((state) => ({ ...state, status: undefined, message: '' }));
	},

	setReset() {
		setTokenStorage('');
		set(() => ({
			user: null,
			message: '',
			isAuthenticated: false,
			authName: undefined,
			status: undefined,
		}));
	},
}));

authStore.subscribe((state) => {
	setTokenStorage(state.token);
});

export default authStore;
