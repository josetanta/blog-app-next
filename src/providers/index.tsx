import { ReactNode, useMemo } from 'react';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { ThemeProvider } from '@material-ui/core/styles';
import { ReactQueryDevtools } from 'react-query/devtools';
import { SettingsProvider } from 'context/SettingsContext';

import { CssBaseline } from '@material-ui/core';
import appTheme from 'utils/appTheme';

const queryClient = new QueryClient();

const isEnvDev = process.env.NODE_ENV === 'development';

type GeneralProviderProps = { pageProps: any; children: ReactNode };

function GeneralProvider(props: GeneralProviderProps) {
	const themeMemo = useMemo(() => appTheme, []);
	return (
		<QueryClientProvider client={queryClient} key='client-provider'>
			<Hydrate state={props.pageProps.dehydratedState}>
				<SettingsProvider>
					<ThemeProvider theme={themeMemo}>
						<CssBaseline />
						{props.children}
					</ThemeProvider>
				</SettingsProvider>

				{isEnvDev && <ReactQueryDevtools position='bottom-right' />}
			</Hydrate>
		</QueryClientProvider>
	);
}

export default GeneralProvider;
