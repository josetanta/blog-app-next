const LOGIN_0AUTH = `${process.env.NEXT_PUBLIC_API_URL}/api/login/auth/`;

export function loginAuthURI(nameProvider: string): void {
	window.location.replace(LOGIN_0AUTH + nameProvider);
}
