export const HOME = '/',
	ABOUT = '/about',
	SIGN_IN = '/sign/:type',
	REGISTER = '/register';

export const USER_PRINCIPAL = '/user',
	USER_ACCOUNT = '/account-info',
	USER_PROFILE = '/profile',
	USER_SETTINGS = '/settings',
	USER_CHANGE_PASS = '/change-password',
	USER_ARCHIVE_POSTS = '/archive-posts',
	USER_MY_POSTS = '/my-posts';

export const POST_PRINCIPAL = '/publicaciones',
	POST_DETAIL = '/detail/:postId',
	POST_CREATE = '/new-post';

export const NOT_FOUND = '/:rest*',
	FORBIDDEN = '/forbidden',
	UNAUTHORIZED = '/unauthorized';
