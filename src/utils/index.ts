export function setTokenStorage(value: string) {
	if (typeof window !== 'undefined') window.sessionStorage.setItem('token', value);
}

export function getTokenSessionStorage() {
	let value = '';
	if (typeof window !== 'undefined') {
		value = window.sessionStorage.token as string;
	}
	return value;
}
