export function valuesObject<T extends object>(obj: T): FormData {
	const formData = new FormData();
	const newOjb = obj as Record<string, string>;

	for (let key of Object.keys(newOjb)) {
		if (newOjb[key].length > 3) {
			formData.append(key, newOjb[key]);
		}
	}
	return formData;
}
