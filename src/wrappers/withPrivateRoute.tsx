import { FC } from 'react';
import { NextRouter, withRouter } from 'next/router';
import { useAuth } from 'hooks/useStores';

const loginUrl = '/sign/in?redirect=true';

function withPrivateRoute<P = {}>(WrapperComponent: FC<P>) {
	function ComponentPrivate(props: P & { router: NextRouter }) {
		const [auth] = useAuth();

		const router = props.router;

		if (!auth.isAuthenticated) {
			(async () => {
				await router.replace(loginUrl);
			})();
		}

		return <WrapperComponent {...props} />;
	}

	ComponentPrivate.displayName = `${WrapperComponent.name}-Private`;

	return withRouter(ComponentPrivate);
}

export default withPrivateRoute;
