import { FC, StrictMode } from 'react';

function withStrictMode<P = {}>(WrapperComponent: FC<P>) {
	function WC(props: P) {
		let envDev = process.env.NODE_ENV === 'development';

		if (envDev) {
			return (
				<StrictMode>
					<WrapperComponent {...props} />
				</StrictMode>
			);
		} else {
			return <WrapperComponent {...props} />;
		}
	}

	WC.displayName = `Wrapper${WrapperComponent.name}`;

	return WC;
}

export default withStrictMode;
