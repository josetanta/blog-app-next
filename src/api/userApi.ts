import { ResponseApi, UserAccountResponse, UserDetailsResponse } from 'response-models';
import { clientHttp } from './config';

export async function accountUpdate(values: any): ResponseApi<UserAccountResponse> {
	try {
		return await clientHttp.patch('/users/account/', values);
	} catch (error: any) {
		return error.response;
	}
}

export async function userDetails(userId: string): ResponseApi<UserDetailsResponse> {
	try {
		return await clientHttp.get<UserDetailsResponse>(`/users/${userId}/details/`);
	} catch (error: any) {
		return error.response;
	}
}
