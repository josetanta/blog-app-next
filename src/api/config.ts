import axios from 'axios';
import { getTokenSessionStorage } from 'src/utils';

const API_URL = process.env.NEXT_PUBLIC_API_URL || '';
const API_VERSION = process.env.NEXT_PUBLIC_API_VERSION || '';

// Config Default
axios.defaults.baseURL = `${API_URL}/api${API_VERSION}`;
axios.defaults.headers.post['Content-Type'] = 'application/json;';
axios.defaults.withCredentials = true;
axios.defaults.xsrfCookieName = 'jwt';

const clientHttp = axios.create();

clientHttp.defaults.headers.common['Authorization'] = `Bearer ${getTokenSessionStorage()}`;

export { clientHttp };
