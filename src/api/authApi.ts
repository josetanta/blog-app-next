import { LoginData } from 'required-params';
import { MessageResponse, ResponseApi, UserAccountResponse } from 'response-models';
import { getTokenSessionStorage } from 'utils/index';
import { clientHttp } from './config';

export async function authLogin(data: LoginData): ResponseApi<MessageResponse & { token: string }> {
	try {
		return await clientHttp.post('/login/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function authLogout(authName?: string): ResponseApi {
	try {
		return await clientHttp.post(authName ? `/auth/logout/${authName}` : '/logout/');
	} catch (error: any) {
		return error.response;
	}
}

export async function authGetUserAccount(token: string): ResponseApi<UserAccountResponse> {
	try {
		return await clientHttp.get('/users/account/', {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		});
	} catch (error: any) {
		return error.response;
	}
}

export async function authRegister(data: any): ResponseApi {
	try {
		return await clientHttp.post('/register/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function getTokenAuth(): ResponseApi<{ token: string }> {
	try {
		return await clientHttp.get(`/auth/token`, {
			headers: {
				Authorization: `Bearer ${getTokenSessionStorage()}`,
			},
		});
	} catch (error: any) {
		return error.response;
	}
}
