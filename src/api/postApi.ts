import {
	Post,
	PostDeleteResponse,
	PostListResponse,
	PostResponse,
	ResponseApi,
} from 'response-models';
import { clientHttp } from './config';

export async function postGetList(): ResponseApi<PostListResponse> {
	return await clientHttp.get<PostListResponse>('/posts/');
}

export async function postGetDetail(postId: string): ResponseApi<PostResponse> {
	return await clientHttp.get<PostResponse>(`/posts/${postId}`);
}

export async function createPost(data: object | FormData | Post): ResponseApi<PostResponse> {
	try {
		return await clientHttp.post<PostResponse>('/posts/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function removePost(postId: string): ResponseApi<PostDeleteResponse> {
	return await clientHttp.delete(`/posts/deleted/?post_id=${postId}&deleted=1`);
}
