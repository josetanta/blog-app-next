import { useMutation } from 'react-query';
import { authLogin, authLogout } from 'api/authApi';

import { mutations } from 'utils/queryConstants';

export function useLogin() {
	return useMutation(mutations.USER_LOGIN_MUT, authLogin, {
		onMutate: async () => {},
	});
}

export function useLogout() {
	return useMutation(mutations.USER_LOGOUT_MUT, authLogout, {
		onMutate: async () => {},
	});
}
