import { useCallback, useEffect, useRef } from 'react';

// eslint-disable-next-line no-unused-vars
type Callback = (isMounted: boolean) => Promise<void> | void;

/**
 * Hook mounted on component
 * @returns isMounted
 * @param fn
 */
function useMounted(fn?: Callback) {
	let isMountedRef = useRef(true);
	let call = useCallback(() => {
		fn && fn(isMountedRef.current);
	}, [fn]);

	useEffect(() => {
		call();
		return () => {
			isMountedRef.current = false;
		};
	}, [call]);

	return isMountedRef.current;
}

export default useMounted;
