import { createContext, ReactNode, useContext, useReducer } from 'react';
import { settingsReducer, SettingsState } from './reducers';

type SettingsContextType = {
	state: SettingsState;
	changeViewPosts(): void;
	// eslint-disable-next-line no-unused-vars
	changeFile(file: File): void;
	removeFile(): void;
};
const settingsContext = createContext<SettingsContextType>({} as SettingsContextType);

const initState: SettingsState = {
	file: undefined,
	isGridView: true,
};

export function SettingsProvider(props: { children: ReactNode }) {
	const [state, dispatch] = useReducer(settingsReducer, initState, (s) => ({ ...s }));

	const changeViewPosts = () => {
		dispatch({ type: 'CHANGE_VIEW_POSTS' });
	};

	const changeFile = (file: File) => {
		dispatch({ type: 'SUBMIT_FILE', payload: file });
	};

	const removeFile = () => {
		dispatch({ type: 'REMOVE_FILE' });
	};

	return (
		<settingsContext.Provider
			value={{
				state,
				changeViewPosts,
				changeFile,
				removeFile,
			}}
		>
			{props.children}
		</settingsContext.Provider>
	);
}

export const SettingsConsumer = settingsContext.Consumer;

export function useUserSettings() {
	return useContext(settingsContext);
}
